/* global b, d, f, h, m, s, x, C, T */

/**
 * b === document.body
 * d === manual adder input
 * f === format function
 * h === <h1>
 * m === highest index to show
 * s === tick counter
 * x === resource names AND container for resources (combined in one object)
 *
 * p{resource name} === container for resource el
 * b{resource name} === button for resource name
 * c{resource name} === resource count
 *
 * C === costs
 * T === global tick function
 */

/**
 * Format an amount of a resource.
 */
f = (amount, word) =>
  amount + " " + word.replace(/_/g, " ").slice(0, amount ^ 1 ? 32 : -1);

x = [
  "bytes",
  "computers",
  "programmers",
  "random_number_generators",
  "network_cables",
  "computer_factorys",
  "internets",
];

x.map((resourceName) => {
  x[resourceName] = m = s = 0;
});

{
  b.innerHTML = "<style>*{line-height:3rem;font-family:monospace";

  b.innerHTML += "<h1 id=h>";

  b.innerHTML += '<input id=d placeholder="type bytes here">';

  x.slice(1).map((resourceName) => {
    b.innerHTML +=
      "<p id=p" + resourceName + "><button id=b" + resourceName + ">";
    this["p" + resourceName].innerHTML += "<b id=c" + resourceName + ">";
  });
}

(T = () => {
  s = (s + 1) % 32;

  if (s < x.computers + 1 && d.value.length) {
    d.value = d.value.slice(x.computers + 1);
    x.bytes += x.computers + 1;
  }

  x.bytes +=
    x.programmers * Math.max(x.network_cables, 1) +
    Math.ceil(Math.random() * 512 * x.random_number_generators) +
    (x.internets ? 2 ** (x.internets * 8) : 0);

  x.computers += (Math.random() < 0.1) * x.computer_factorys;

  C = {
    computers: {
      bytes: Math.min(
        31 + Math.ceil(1.3 ** x.computers + 1.5 * x.computers),
        4294967296
      ),
    },
    programmers: {
      bytes: Math.ceil(127 + 1.8 ** x.programmers + 128 * x.programmers),
    },
    random_number_generators: {
      computers: 1,
      programmers: 8 * Math.ceil(1.05 ** x.random_number_generators),
    },
    network_cables: {
      bytes: Math.ceil(1.5 ** x.network_cables),
      computers: 2,
    },
    computer_factorys: {
      bytes: 2 ** 20 - 1 + Math.ceil(1.2 ** x.computer_factorys),
    },
    internets: {
      computers: 128,
      network_cables: 32,
    },
  };

  h.innerHTML = f(x.bytes, "bytes");

  x.slice(1).map((resourceName, index) => {
    let resourceCount = x[resourceName];

    let resourceCosts = C[resourceName];

    this["p" + resourceName].hidden = m < index;

    this["b" + resourceName].innerHTML =
      f(1, resourceName) +
      ` (cost: ${x
        .filter((r) => resourceCosts[r])
        .map((r) => f(resourceCosts[r], r)).join`, `})`;
    this[`b${resourceName}`].onclick = () => {
      if (
        resourceName == "internets" &&
        !confirm("Made the internet! Keep playing?")
      ) {
        location.reload();
      }

      x[resourceName]++;
      x.map((otherResourceName) => {
        x[otherResourceName] -= resourceCosts[otherResourceName] || 0;
      });
      m = Math.max(m, index + 1);
    };
    this["b" + resourceName].disabled = x.some(
      (otherResourceName) =>
        (resourceCosts[otherResourceName] || 0) > x[otherResourceName]
    );
    this["c" + resourceName].innerHTML = resourceCount;
  });

  requestAnimationFrame(T);
})();
