const express = require("express");
const { RegPack } = require("regpack");
const { minify } = require("terser");
const path = require("path");
const fs = require("fs/promises");
const minBy = require("lodash/minBy");

async function getSubmissionHtml({ dev } = {}) {
  const filePath = path.join(__dirname, "..", "submission", "index.js");
  const rawJs = (await fs.readFile(filePath, "utf8")).trim();

  let js;
  if (dev) {
    js = rawJs;
  } else {
    const tersered = (
      await minify(rawJs, {
        compress: {
          arguments: true,
          booleans_as_integers: true,
          drop_console: true,
          ecma: 2016,
          keep_fargs: false,
          passes: 4,
          pure_funcs: [],
          pure_getters: true,
          unsafe: true,
          unsafe_arrows: true,
          unsafe_comps: true,
          unsafe_Function: true,
          unsafe_math: true,
          unsafe_symbols: true,
          unsafe_methods: true,
          unsafe_proto: true,
          unsafe_regexp: true,
          unsafe_undefined: true,
        },
        mangle: {
          toplevel: true,
        },
      })
    ).code;

    const regpackResults = new RegPack().runPacker(tersered, {
      varsNotReassigned: ["b"],
      crushGainFactor: 8,
      crushLengthFactor: 2,
      crushCopiesFactor: 8,
      crushTiebreakerFactor: 2,
    })[0].result;
    const bestRegpack = minBy(regpackResults, (r) => r[0])[1];

    js = minBy([rawJs, tersered, bestRegpack], "length");
  }

  return `<body id=b><script>${js}</script>`;
}

const app = express();

app.get("/", async (req, res, next) => {
  res.type("html");
  try {
    res.send(await getSubmissionHtml({ dev: Boolean(req.query.dev) }));
  } catch (err) {
    next(err);
  }
});

getSubmissionHtml()
  .then((htmlAtServerStart) => {
    const size = Buffer.byteLength(htmlAtServerStart);
    console.log(`at server start, submission was ${size} bytes`);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });

app.listen(3000, () => {
  console.log("development server started");
});
